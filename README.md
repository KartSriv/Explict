# Explict
A small program with large abilities.

This is a program which is used to add malware to windows.
NOTE: This is just for educational purpose(s) only!

Step 1: Make a live distro:

![alt text](https://raw.githubusercontent.com/karthiksrivijay/Explict/master/Yumi.png)

NOTE: I used YUMI by pendrivelinux.com (A very stable application. Trust me!) To download [click here!](https://www.pendrivelinux.com/yumi-multiboot-usb-creator/)

After the live disk, boot it and run the program [EnableExplict.py](https://github.com/karthiksrivijay/Explict/blob/master/EnableExplict.py)

The program will guide you what to do.
After the hack you will see this window in the booting part of windows 

![alt text](https://raw.githubusercontent.com/karthiksrivijay/Explict/master/AfterHack.png)

and then edit the [EnableExplict.py](https://github.com/karthiksrivijay/Explict/blob/master/EnableExplict.py) file in your favarite text editor and then get. set. hack.

This is a GNU licenced project so you can edit it but as a author I can expect atleast a credits for the project.
